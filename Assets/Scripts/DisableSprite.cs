using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class DisableSprite : MonoBehaviour
{
    [SerializeField] private bool disable = true;
    
    private void Awake()
    {
        if (disable) gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }
}
