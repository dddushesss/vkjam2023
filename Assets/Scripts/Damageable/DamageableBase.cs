﻿using System;
using UnityEngine;

namespace Damageable
{
    public class DamageableBase : MonoBehaviour, IDamageable
    {
        [SerializeField] protected float fullHealth = 25;
        public event EventHandler OnHealthChanged;

        protected float Health;

        public float GetFullHealth() => fullHealth;
        public float GetHealth() => Health;
        private void Start() => Init();
        protected virtual void Init()
        {
            Health = fullHealth;
        }
        
        public void Damage(float points)
        {
            Health -= points;
            OnHealthChanged?.Invoke(this, EventArgs.Empty);

            if (Health <= 0)
            {
                Die();
            }
        }

        protected virtual void Die()
        {
            Destroy(gameObject);
        }
    }
}