using System;
using UnityEngine;
using UnityEngine.UI;

namespace Damageable
{
    public class HealthView : MonoBehaviour
    {
        [SerializeField] private Transform healthBarRoot;
        [SerializeField] private Image healthBar;
        [SerializeField] private Vector3 offset;
        
        private IDamageable _damageable;
        private Camera _camera;

        private void Start()
        {
            _camera = Camera.main;
            _damageable = transform.parent.GetComponent<IDamageable>();
            _damageable.OnHealthChanged += HandleHealthChanged;
        }

        private void HandleHealthChanged(object sender, EventArgs e)
        {
            healthBar.fillAmount = _damageable.GetHealth() / _damageable.GetFullHealth();
        }

        private void Update()
        {
            healthBarRoot.position = _camera.WorldToScreenPoint(transform.parent.position + offset);
        }

        private void OnDestroy()
        {
            _damageable.OnHealthChanged -= HandleHealthChanged;
        }
    }
}
