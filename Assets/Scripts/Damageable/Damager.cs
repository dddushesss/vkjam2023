﻿using UnityEngine;

namespace Damageable
{
    public class Damager : MonoBehaviour
    {
        [SerializeField] protected float damagePoints = 10;

        private void Start() => Init();
        protected virtual void Init() { }

        private void OnCollisionEnter2D(Collision2D col)
        {
            if (!col.gameObject.TryGetComponent<IDamageable>(out var damageable)) return;
            
            damageable = col.gameObject.GetComponent<IDamageable>();
            ApplyDamage(damageable, col);
        }

        protected virtual void ApplyDamage(IDamageable damageable, Collision2D col)
        {
            damageable.Damage(damagePoints);
            Destroy();
        }

        protected virtual void Destroy()
        {
            Destroy(gameObject);
        }
    }
}