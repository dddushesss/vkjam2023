using System;

namespace Damageable
{
    public interface IDamageable
    {
        event EventHandler OnHealthChanged;
        void Damage(float points);
        float GetFullHealth();
        float GetHealth();
    }
}
