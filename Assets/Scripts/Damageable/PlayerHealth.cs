using System;
using Pickables;
using UnityEngine;

namespace Damageable
{
    public class PlayerHealth : DamageableBase
    {
        [SerializeField] private GameEvents gameEvents;
        [SerializeField] private PickableEvents pickableEvents;

        protected override void Init()
        {
            base.Init();
            
            if (gameEvents == null) Debug.LogError("gameEvents is not assigned", this);
            if (pickableEvents == null) Debug.LogError("pickableEvents is not assigned", this);

            pickableEvents.OnHealthAdd += AddHealth;
        }

        private void AddHealth(float points)
        {
            Health += points;
            if (Health > fullHealth) fullHealth = Health;
            Debug.Log("Health added " + points);
        }

        protected override void Die()
        {
            gameEvents.OnPlayerDied?.Invoke();
        }

        private void OnDestroy()
        {
            pickableEvents.OnHealthAdd -= AddHealth;
        }
    }
}
