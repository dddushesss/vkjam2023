﻿using UnityEngine;

namespace Damageable
{
    public class EnemyHealth : DamageableBase
    {
        protected override void Die()
        {
            Debug.Log("Enemy Destroyed");
            Destroy(gameObject);
        }
    }
}