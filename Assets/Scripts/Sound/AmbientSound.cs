﻿using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using Enemies;
using UnityEngine;

namespace Sound
{
    public class AmbientSound : MonoBehaviour
    {
        [Serializable]
        class BossAudioClip
        {
            public BossTypes Type;
            public AudioClip Begining;
            public AudioClip Loop;
        }
        
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private List<BossAudioClip> bossAudioClips;

        private BossAudioClip _current;

        private void Start()
        {
            GlobalStorage.Instance.LevelGenerator.OnBossAreaChanged += ChangeAmbientSound;
        }

        private void ChangeAmbientSound(BossTypes boss)
        {
            var bossAudioClip = bossAudioClips.FirstOrDefault(bossA => bossA.Type == boss);
            if (bossAudioClip == null)
            {
                Debug.LogError($"Cannot find bossAudioClip for {boss}");
                return;
            }
            Debug.Log($"Change audio clip to {boss}");

            _current = bossAudioClip;
            
            audioSource.clip = _current.Begining;
            audioSource.Play();
            
            CancelInvoke();
            Invoke(nameof(ChangeToLoop), _current.Begining.length);
        }

        private void ChangeToLoop()
        {
            audioSource.clip = _current.Loop;
            audioSource.loop = true;
            audioSource.Play();
        }

        private void OnDestroy()
        {
            GlobalStorage.Instance.LevelGenerator.OnBossAreaChanged -= ChangeAmbientSound;
            
            CancelInvoke();
        }
    }
}