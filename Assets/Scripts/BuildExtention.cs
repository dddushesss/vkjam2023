﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public static partial class BuildExtention
{
    public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
    {
        var res = gameObject.GetComponent<T>();
        if (res == null)
        {
            res = gameObject.AddComponent<T>();
        }

        return res;
    }

    public static T GetRand<T>(this T[] array)
    {
        var index = UnityEngine.Random.Range(0, array.Length - 1);
        return array[index];
    }

    public static T PopRand<T>(this List<T> list)
    {
        var index = UnityEngine.Random.Range(0, list.Count - 1);
        var res = list[index];
        list.RemoveAt(index);
        return res;
    }

    public static GameObject[] GetAllChilderns(this GameObject gameObject)
    {
        var res = new List<GameObject>();
        
        foreach (Transform child in gameObject.transform)
        {
            res.Add(child.gameObject);
            res.AddRange(GetAllChilderns(child.gameObject));
        }
        return res.ToArray();
    }

}