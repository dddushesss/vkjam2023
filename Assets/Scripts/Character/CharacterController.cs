﻿using UnityEngine;

namespace Character
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class CharacterController : MonoBehaviour
    {
        [SerializeField] [Tooltip("The speed at which the character will move.")]
        private float moveSpeed = 1f;

        [SerializeField] [Tooltip("The speed at which the character will accelerate.")]
        private float moveAcceleration = 1f;

        [SerializeField] [Tooltip("The force at which the character will jump.")]
        private float jumpForce = 1f;

        [SerializeField] private KeyCode jumpKey = KeyCode.W;

        [SerializeField] private KeyCode fallKey = KeyCode.S;

        [SerializeField] private LayerMask mask;

        [SerializeField] private AudioSource walkAudioSource;

        private Rigidbody2D _rigidbody2D;
        private Animator _animator;
        private SpriteRenderer _spriteRenderer;

        private Vector2 moveDir;
        private float _speed;
        private bool _isLanded = true;
        private CapsuleCollider2D _capsuleCollider2D;
        private static readonly int IsJumping = Animator.StringToHash("isJumping");
        private static readonly int IsWalking = Animator.StringToHash("isWalking");
        private static readonly int IsFalling = Animator.StringToHash("isFalling");

        private void Start()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
            _capsuleCollider2D = GetComponent<CapsuleCollider2D>();
            _animator = GetComponent<Animator>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            var dt = Time.deltaTime;
            if (Input.GetKeyDown(jumpKey) && _isLanded)
            {
                Jump();
            }

            if (Input.GetKeyDown(fallKey) && _isLanded)
            {
                Fall();
            }

            var input = Input.GetAxis("Horizontal");
            _spriteRenderer.flipX = input > 0;
            _speed = Mathf.Lerp(_speed, moveSpeed * input, moveAcceleration * dt);
            _animator.SetBool(IsWalking, Mathf.Abs(input) > 0);
            _animator.SetBool(IsJumping, !_isLanded && _rigidbody2D.velocity.y > 0);
            _animator.SetBool(IsFalling, !_isLanded && _rigidbody2D.velocity.y < 0);
        }

        private void FixedUpdate()
        {
            _rigidbody2D.AddForce(new Vector2(_speed, _rigidbody2D.velocity.y), ForceMode2D.Impulse);

            _isLanded = Physics2D.BoxCast(transform.position - new Vector3(0, _capsuleCollider2D.size.y + 0.01f),
                new Vector2(_capsuleCollider2D.size.x + 0.1f, 0.1f), 0f, Vector2.down, 0.3f, mask);
        }

        private void Jump()
        {
            _rigidbody2D.AddForce(new Vector2(0, jumpForce));
        }

        private void Fall()
        {
            Debug.Log("Fall");
        }

        private void WalkPlaySound()
        {
            walkAudioSource.Play();
        }
    }
}