using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Character
{
    public class LevitationController : MonoBehaviour
    {
        [SerializeField] private KeyCode pickObjectButton = KeyCode.Mouse1;
        [SerializeField] private float forceMultiplier = 100f;
        [SerializeField] private float maxThrowForce = 600f;
        [SerializeField] private float holdTime = 0.5f;
        [SerializeField] private SpringJoint2D mouseGameObjectPrefab;
        
        [SerializeField] private float breakForce = 50f;
        [SerializeField] private float maxDistance = 500f;
        [SerializeField] private float dampingRatio = 0f;
        [SerializeField] private LayerMask layerMask;

        private bool _isObjectPicked = false;
        private bool _isKeyPressed = false;
        private bool _isThrowing = false;

        private Rigidbody2D _pickedObject;
        private Camera _camera;
        private float _keyHoldTime = 0f;
        private GameObject _mouseGameObject;
        private SpringJoint2D _mouseGameObjectJoint;
        private Rigidbody2D _mouseGameObjectRB;

        private void Start()
        {
            _camera = Camera.main;
            _mouseGameObject = Instantiate(mouseGameObjectPrefab.gameObject);
            _mouseGameObjectRB = _mouseGameObject.GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            Vector3 mousePosition = _camera.ScreenToWorldPoint(Input.mousePosition);
            _mouseGameObjectRB.MovePosition(mousePosition);
            _keyHoldTime += Time.deltaTime;
            if (Input.GetKey(pickObjectButton) && !_isKeyPressed)
            {
                _keyHoldTime = 0f;
                HandleOnPickupActionPressed();
            }
            else if (!Input.GetKey(pickObjectButton) && _isKeyPressed)
            {
                HandleOnPickupActionReleased();
            }
            
            if (_isKeyPressed && _isObjectPicked && _keyHoldTime >= holdTime)
            {
                _isThrowing = true;
                _pickedObject.bodyType = RigidbodyType2D.Static;
            }

            if (_isObjectPicked && (_mouseGameObjectJoint == null || _mouseGameObjectJoint.connectedBody == null))
            {
                DetachObject();
            } 
        }
        private void HandleOnPickupActionPressed()
        {
            _isKeyPressed = true;
           
        }  
        private void HandleOnPickupActionReleased()
        {
            _isKeyPressed = false;
            
            if (!_isObjectPicked)
            {
                AttachObject();
            }
            else
            {
               DetachObject();
            }
        }

        private void AttachObject()
        {
            Vector3 mousePosition = _camera.ScreenToWorldPoint(Input.mousePosition);
            var target = Physics2D.OverlapPoint(mousePosition, layerMask);
            target?.gameObject.TryGetComponent(out _pickedObject);
            if (_pickedObject == null) return;
            _isObjectPicked = true;
            _mouseGameObjectJoint = _mouseGameObject.GetOrAddComponent<SpringJoint2D>();
            _mouseGameObjectJoint.breakForce = breakForce;
            _mouseGameObjectJoint.distance = maxDistance;
            _mouseGameObjectJoint.dampingRatio = dampingRatio;
            
            _mouseGameObjectJoint.connectedBody = _pickedObject;
        }

        private void DetachObject()
        {
            if (_isThrowing)
            {
                _pickedObject.bodyType = RigidbodyType2D.Dynamic;
                _pickedObject.WakeUp();
                Vector3 mousePosition = _camera.ScreenToWorldPoint(Input.mousePosition);
                var throwDirection = _pickedObject.transform.position - new Vector3(mousePosition.x, mousePosition.y);
                var force = throwDirection.magnitude * forceMultiplier;
                force = Mathf.Clamp(force, 0, maxThrowForce);
                Debug.Log(force);
                _pickedObject.AddForce(throwDirection.normalized * force);
                _isThrowing = false;
            }

            if (_mouseGameObjectJoint != null)
            {
                _mouseGameObjectJoint.connectedBody = null;
            }

            _isObjectPicked = false;
            _pickedObject = null;
        }

        private void OnDrawGizmos()
        {
            if (!_isObjectPicked)
            {
                return;
            }

            Vector3 mousePosition = _camera.ScreenToWorldPoint(Input.mousePosition);
            Gizmos.color = Color.green;
            var position = _pickedObject.gameObject.transform.position;
            Gizmos.DrawLine(position, new Vector3(mousePosition.x, mousePosition.y, 0f));

            Gizmos.color = Color.red;
        }
    }
}