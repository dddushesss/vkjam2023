﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Character
{
    public class PhysicsDisableOutOfRadius : MonoBehaviour
    {
        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.attachedRigidbody == null)
                return;
            other.attachedRigidbody.bodyType = RigidbodyType2D.Static;
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.attachedRigidbody == null)
                return;
            other.attachedRigidbody.bodyType = RigidbodyType2D.Dynamic;
        }
    }
}