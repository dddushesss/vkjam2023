﻿using System;
using UnityEngine;

namespace Character
{
    public class CharacterSpawner : MonoBehaviour
    {
        [SerializeField] private CharacterController _characterPrefab;

        public GameObject Spawn()
        {
            if (_characterPrefab == null)
            {
                Debug.LogError("CharacterSpawner: Character prefab is null");
                return null;
            }

            var character = Instantiate(_characterPrefab, transform.position, Quaternion.identity);
            Camera.main.GetComponent<CameraFollow>().target = character.transform;
            return character.gameObject;
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawIcon(transform.position, "CharacterSpawner");
        }
    }
}