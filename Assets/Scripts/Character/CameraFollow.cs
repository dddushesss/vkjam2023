﻿using UnityEngine;

namespace Character
{
    public class CameraFollow : MonoBehaviour
    {
        public Transform target;
        
        public float smoothSpeed = 0.125f;
        public Vector3 offset;
        
        private void FixedUpdate ()
        {
            if (target == null)
                return;
            var desiredPosition = target.position + offset;
            var smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.fixedDeltaTime);
            transform.position = smoothedPosition;
        }
    }
}