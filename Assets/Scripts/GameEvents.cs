﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvents", menuName = "GameEvents", order = 51)]
public class GameEvents : ScriptableObject
{
    public Action OnPlayerDied;
    public Action OnEndGame;
}