using System;
using UnityEngine;

namespace Enemies
{
    public class AreaDetection : MonoBehaviour
    {
        public Action<Collider2D> OnEnter;
        public Action<Collider2D> OnExit;

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.isTrigger) return;
            
            // Debug.Log("Player trigger entered");
            OnEnter?.Invoke(col);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.isTrigger) return;

            // Debug.Log("Player exited");
            OnExit?.Invoke(other);
        }
    }
}
