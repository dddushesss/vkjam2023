using UnityEngine;

namespace Enemies
{
    [RequireComponent(typeof(SpriteRenderer), typeof(Rigidbody2D))]
    public class EnemyMovement : MonoBehaviour
    {
        [SerializeField] private float speed = 3;
        [SerializeField] private float distanceToTarget = 4;
        [SerializeField] private float offset = 1;

        private Rigidbody2D _rb;
        private SpriteRenderer _spriteRenderer;
        private AreaDetection _areaDetection;
        private Collider2D _target;
        private bool _isMovingToPlayer;

        private void Start()
        {
            _rb = GetComponent<Rigidbody2D>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _areaDetection = transform.GetComponentInChildren<AreaDetection>();

            _areaDetection.OnEnter += HandleOnPlayerEnter;
            _areaDetection.OnExit += HandleOnPlayerExit;
        }

        private void HandleOnPlayerEnter(Collider2D playerCol)
        {
            _target = playerCol;
            _isMovingToPlayer = true;
        }

        private void HandleOnPlayerExit(Collider2D playerCol)
        {
            _target = null;
        }

        private void FixedUpdate()
        {
            if (_target == null || _rb.bodyType == RigidbodyType2D.Static) return;
        
            var distance = (_target.transform.position - transform.position).magnitude;
            if (distance < distanceToTarget)
            {
                _isMovingToPlayer = false;
            }
        
            var move = false;
            if (_isMovingToPlayer)
            {
                move = true;
            }
            else if (distance > distanceToTarget + offset)
            {
                _isMovingToPlayer = true;
            }

            if (move)
            {
                var dir = _target.transform.position - transform.position;
                
                var x = Mathf.Clamp(dir.x, -1, 1);
                var y = Mathf.Clamp(dir.y, -1, 1);
                _rb.velocity = new Vector2(speed * x, speed * y);
                
                _spriteRenderer.flipX = x > 0;
            }
            else
            {
                _rb.velocity = new Vector2(0, 0);
            }
        }

        private void OnDestroy()
        {
            _areaDetection.OnEnter -= HandleOnPlayerEnter;
            _areaDetection.OnExit -= HandleOnPlayerExit;
        }
    }
}
