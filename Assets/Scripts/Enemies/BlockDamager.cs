﻿using Damageable;
using UnityEngine;

namespace Enemies
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class BlockDamager : Damager
    {
        [SerializeField] private float damageScale = 0.1f;
        [SerializeField] private float minDamageSpeed = 3;
        [SerializeField] private float minDamagePoints = 0;
        [SerializeField] private float maxDamagePoints = 100;

        private Rigidbody2D _rb;

        protected override void Init()
        {
            base.Init();

            _rb = GetComponent<Rigidbody2D>();
        }

        protected override void ApplyDamage(IDamageable damageable, Collision2D col)
        {
            if (_rb == null) return;
            
            var speed = _rb.velocity.magnitude;
            if (speed < minDamageSpeed) return;

            var points = Mathf.Clamp(damagePoints * speed * damageScale, minDamagePoints, maxDamagePoints);
            damageable.Damage(points);
        }
    }
}