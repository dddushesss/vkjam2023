﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Damageable;
using UnityEngine;

namespace Enemies
{
    public class TrapDamager : MonoBehaviour
    {
        [SerializeField] protected float damagePoints = 10;
        [SerializeField] private float repeatRate = 1;

        private readonly List<GameObject> _targets = new();
        private readonly List<GameObject> _removed = new();

        private void Start() => Init();

        protected virtual void Init()
        {
        }

        private void OnCollisionEnter2D(Collision2D col)
        {
            var target = col.gameObject;
            if (!target.TryGetComponent<IDamageable>(out var damageable)) return;
            
            if (_targets.Contains(target)) return;
            
            _targets.Add(target);
            if (_targets.Count == 1)
            {
                InvokeRepeating(nameof(ApplyDamageToAll), repeatRate, repeatRate);
            }
            
            if (!_removed.Contains(target)) ApplyDamage(damageable, target);
        }

        private void OnCollisionExit2D(Collision2D col)
        {
            var target = col.gameObject;
            if (!target.TryGetComponent<IDamageable>(out var damageable)) return;
            
            _targets.Remove(target);
            RememberRemoved(target);

            if (_targets.Count == 0)
            {
                CancelInvoke();
            }
        }

        private async void RememberRemoved(GameObject target)
        {
            _removed.Add(target);
            await Task.Delay((int)(1000 * repeatRate));
            _removed.Remove(target);
        }

        private void ApplyDamageToAll()
        {
            foreach (var target in _targets.Where(target => target != null))
            {
                ApplyDamage(target.GetComponent<IDamageable>(), target);
            }
        }
        
        protected void ApplyDamage(IDamageable damageable, GameObject target)
        {
            if (target.CompareTag("Player") || target.CompareTag("BotEnemy"))
            {
                damageable.Damage(damagePoints);
            }
        }

        private void OnDestroy()
        {
            CancelInvoke();
        }
    }
}