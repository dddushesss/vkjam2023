﻿namespace Enemies
{
    public enum BossTypes
    {
        NONE,
        STARVE,
        WAR,
        DEATH,
        PLAGUE
    }
}