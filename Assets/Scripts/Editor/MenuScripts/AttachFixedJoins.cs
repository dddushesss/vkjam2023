﻿using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

namespace MenuScripts
{
    public class AttachFixedJoins
    {
        [MenuItem("GameObject/Joins/Attach #a", false, 20)]
        private static void AttachFixedJoin(MenuCommand menuCommand)
        {
            var selectedGameObjects = Selection.gameObjects;
        
            if (selectedGameObjects == null || selectedGameObjects.Length == 0)
            {
                return;
            }

            foreach (var gameObject in selectedGameObjects)
            {
                GameObject attachable = null;
                foreach (var target in selectedGameObjects.Where(obj => obj.GetComponent<Rigidbody2D>()))
                {
                    if (gameObject == target)
                    {
                        continue;
                    }

                    if ((attachable == null || Vector3.Distance(gameObject.transform.position, target.transform.position) <
                         Vector3.Distance(gameObject.transform.position, attachable.transform.position)) && 
                        gameObject.transform.GetComponents<FixedJoint2D>().Count(joint => joint.connectedBody == target.GetComponent<Rigidbody2D>()) == 0 &&
                        target.transform.GetComponents<FixedJoint2D>().Count(joint => joint.connectedBody == gameObject.GetComponent<Rigidbody2D>()) == 0)
                    {
                        attachable = target;
                    }
                }

                if (attachable != null)
                {
                    gameObject.AddComponent<FixedJoint2D>().connectedBody = attachable.GetComponent<Rigidbody2D>();
                }
            }
        }
    }
}