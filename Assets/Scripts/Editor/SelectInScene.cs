﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

public class SelectInScene : MonoBehaviour
{
    [SerializeField] private Transform root;
    [SerializeField] private string wildCard;

    [EditorButton]
    public void Select()
    {
        var selected = new List<GameObject>();

        var gameObjects= root == null ? FindObjectsOfType<GameObject>()
            : root.transform.GetComponentsInChildren<Transform>().Select(tr => tr.gameObject).ToArray();
        
        foreach (var go in gameObjects)
        {
            if (IsWildcardMatch(wildCard, go.name)) selected.Add(go);
        }
        
        Selection.objects = selected.ToArray();
    }
    
    public bool IsWildcardMatch(string wildcardPattern, string subject)
    {
        if (string.IsNullOrWhiteSpace(wildcardPattern))
        {
            return false;
        }

        string regexPattern = string.Concat("^", Regex.Escape(wildcardPattern).Replace("\\*", ".*"), "$");

        int wildcardCount = wildcardPattern.Count(x => x.Equals('*'));
        if (wildcardCount <= 0)
        {
            return subject.Equals(wildcardPattern, StringComparison.CurrentCultureIgnoreCase);
        }
        else if (wildcardCount == 1)
        {
            string newWildcardPattern = wildcardPattern.Replace("*", "");

            if (wildcardPattern.StartsWith("*"))
            {
                return subject.EndsWith(newWildcardPattern, StringComparison.CurrentCultureIgnoreCase);
            }
            else if (wildcardPattern.EndsWith("*"))
            {
                return subject.StartsWith(newWildcardPattern, StringComparison.CurrentCultureIgnoreCase);
            }
            else
            {
                try
                {
                    return Regex.IsMatch(subject, regexPattern);
                }
                catch
                {
                    return false;
                }
            }
        }
        else
        {
            try
            {
                return Regex.IsMatch(subject, regexPattern);
            }
            catch
            {
                return false;
            }
        }
    }
}