using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D),  typeof(SpriteRenderer))]
public class TowerCellView : MonoBehaviour
{
    private Vector2 _currentDirection = Vector2.zero;
    private Rigidbody2D _rigidbody;
    private FixedJoint2D[] _joints;
            
    [EditorButton]
    public void SetForceVector(Vector2 dir)
    {
        _currentDirection = dir;
        _rigidbody.AddForce(dir);
    }

    [EditorButton]
    public void JointPower(float breakForce)
    {
        _joints = GetComponents<FixedJoint2D>();
        foreach (var joint in _joints)
        {
            joint.breakForce = breakForce;
        }
        _joints[0].breakForce = breakForce * 2;
    }

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _joints = GetComponents<FixedJoint2D>();
    }

    private void OnDrawGizmos()
    {
        if (_rigidbody == null)
        {
            return;
        }
        Gizmos.color = Color.yellow;
        var position = gameObject.transform.position;
        Gizmos.DrawLine(position, new Vector2(position.x, position.y) + _rigidbody.velocity);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        _joints ??= GetComponents<FixedJoint2D>();
        
        if (_joints == null)
        {
            return;
        }
        var position = gameObject.transform.position;
        foreach (var joint in _joints)
        {
            if (joint == null || joint.connectedBody == null)
            {
                return;
            }
            Gizmos.DrawLine(position, joint.connectedBody.gameObject.transform.position);
        }
    }
}
