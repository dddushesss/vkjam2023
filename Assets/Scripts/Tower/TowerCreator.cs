using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TowerCreator : MonoBehaviour
{
    [SerializeField]
    private TowerCellView towerCellViewPrefab;
    [SerializeField] private GameObject pin = null;
    [SerializeField] private int _width;
    [SerializeField] private int _height;

    [SerializeField] private float pinBreakForce;
    [SerializeField]private float minBreakForce;
    [SerializeField] private float maxBreakForce;


    [EditorButton]
    public void GenerateTower()
    {
        if (towerCellViewPrefab == null)
        {
            return;
        }
        var cellHeight = towerCellViewPrefab.GetComponent<SpriteRenderer>().bounds.size.y;
        var cellWidth = towerCellViewPrefab.GetComponent<SpriteRenderer>().bounds.size.x;

        var cells = new TowerCellView[_height, _width];
        
        for (int i = 0; i < _height; i++)
        {
            for (int j = 0; j < _width; j++)
            {
                var tower = Instantiate(towerCellViewPrefab, transform);
                tower.transform.SetPositionAndRotation(new Vector3(cellHeight * j, cellWidth * i), Quaternion.identity);
                cells[i, j] = tower;
            }
        }

        for (int i = 0; i < _height; i++)
        {
            for (int j = 0; j < _width; j++)
            {
                var tower = cells[i, j];
                if (pin != null)
                {
                    GameObject _pin = Instantiate(pin, tower.transform);
                    var joint = tower.AddComponent<FixedJoint2D>();
                    joint.connectedBody = _pin.GetComponent<Rigidbody2D>();
                    joint.breakForce = pinBreakForce;
                }
                if (j > 0)
                {
                    var joint = tower.AddComponent<FixedJoint2D>();
                    joint.connectedBody = cells[i, j - 1].GetComponent<Rigidbody2D>();
                    joint.breakForce = Random.Range(minBreakForce, maxBreakForce);
                }
                if (i > 0)
                {
                    var joint = tower.AddComponent<FixedJoint2D>();
                    joint.connectedBody = cells[i - 1, j].GetComponent<Rigidbody2D>();
                    joint.breakForce = Random.Range(minBreakForce, maxBreakForce);
                }

                if (j < _width - 1)
                {
                    var joint = tower.AddComponent<FixedJoint2D>();
                    joint.connectedBody = cells[i, j + 1].GetComponent<Rigidbody2D>();
                    joint.breakForce = Random.Range(minBreakForce, maxBreakForce);
                }
                if (i < _height - 1)
                {
                    var joint = tower.AddComponent<FixedJoint2D>();
                    joint.connectedBody = cells[i + 1, j].GetComponent<Rigidbody2D>();
                    joint.breakForce = Random.Range(minBreakForce, maxBreakForce);
                }
            }
        }
    }
}
