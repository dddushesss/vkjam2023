using UnityEngine;

public class GameMenu : MonoBehaviour
{
    [SerializeField] private KeyCode openMenu = KeyCode.Escape;
    [SerializeField] private GameObject root;

    private bool _menuOpened;
    
    private void Start()
    {
        CloseMenu();
    }

    private void Update()
    {
        if (Input.GetKeyDown(openMenu))
        {
            if (_menuOpened)
            {
                CloseMenu();
            }
            else
            {
                OpenMenu();
            }
        }
    }

    public void CloseMenu()
    {
        Time.timeScale = 1f;
        _menuOpened = false;
        root.SetActive(false);
    }

    public void OpenMenu()
    {
        Time.timeScale = 0f;
        _menuOpened = true;
        root.SetActive(true);
    }

    public void QuitGame()
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }
}
