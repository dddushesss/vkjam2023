﻿using Level;
using UnityEngine;

namespace DefaultNamespace
{
    public class GlobalStorage : MonoBehaviour
    {
        [SerializeField] private PropsTypeLibrary propsTypeLibrary;
        [SerializeField] private BackgroundLibrary backsLibrary;
        public static GlobalStorage Instance { get; private set; }

        public DialogManager DialogManager
        {
            get
            {
                if (_dialogManager == null)
                {
                    _dialogManager = FindObjectOfType<DialogManager>();
                }
                return _dialogManager;
            }
        }

        public LevelGenerator LevelGenerator => _generator;
        public PropsTypeLibrary PropsTypeLibrary => propsTypeLibrary;
        public BackgroundLibrary BacksTypeLibrary => backsLibrary;
        private LevelGenerator _generator;
        private DialogManager _dialogManager;
        
        private void Awake()
        {
            Instance = this;
            _generator = FindObjectOfType<LevelGenerator>();
        }
    }
}