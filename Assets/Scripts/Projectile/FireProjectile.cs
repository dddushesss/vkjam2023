using UnityEngine;

namespace Projectile
{
    public class FireProjectile : MonoBehaviour
    {
        [SerializeField] protected string[] projectiles = new []{ "base" };
        [SerializeField] protected float offset = 1f;

        protected string CurrentProjectile;

        private void Start() => Init();

        protected virtual void Init()
        {
            if (ProjectileFactory.Instance == null)
            {
                Debug.LogError("Set ProjectileFactory in the scene");
            }

            CurrentProjectile = projectiles[0];
        }

        protected void Fire(Vector3 position, Vector3 direction)
        {
            var projectile = ProjectileFactory.Instance.CreateProjectile(CurrentProjectile);
            projectile.transform.position = position + direction.normalized * offset;
            projectile.transform.rotation = GetRotation(direction);
        }

        private Quaternion GetRotation(Vector3 dir)
        {
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            return Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }
}
