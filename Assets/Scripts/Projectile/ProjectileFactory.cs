using UnityEngine;

namespace Projectile
{
    public class ProjectileFactory : MonoBehaviour
    {
        [SerializeField] private ProjectilesSO projectilesSO;
        
        public static ProjectileFactory Instance { get; protected set; }
        
        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(this);
                throw new System.Exception("An instance of this singleton already exists.");
            }
            else
            {
                Instance = this;
            }
            
            projectilesSO.Init();
        }

        public GameObject CreateProjectile(string projectileName)
        {
            return Instantiate(projectilesSO.Projectiles[projectileName], transform);
        }
    }
}
