using Pickables;
using UnityEngine;

namespace Projectile
{
    public class CharacterFireProjectile : FireProjectile
    {
        [SerializeField] private PickableEvents pickableEvents;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private float fireRate = 0.4f;
        
        private Camera _camera;

        protected override void Init()
        {
            base.Init();
            
            if (pickableEvents == null) Debug.LogError("pickableEvents is not assigned", this);
            
            _camera = Camera.main;
            pickableEvents.OnProjectileChange += HandleProjectileChange;
        }

        private void HandleProjectileChange(string projectileName)
        {
            CurrentProjectile = projectileName;
            Debug.Log("ProjectileChanged to " + projectileName);
        }

        private void Update()
        {
            if (Time.timeScale == 0) return;
            
            if (Input.GetMouseButtonDown(0))
            {
                var dir = Input.mousePosition - _camera.WorldToScreenPoint(transform.position);
                Fire(gameObject.transform.position, dir);
                TryFire();
            }
        }


        private float _lastTimeFired = 0;
        private void TryFire()
        {
            var currentTime = Time.realtimeSinceStartup;
            if (currentTime - _lastTimeFired < fireRate) return;
            
            _lastTimeFired = currentTime;
            
            var dir = Input.mousePosition - _camera.WorldToScreenPoint(transform.position);
            Fire(gameObject.transform.position, dir);
            audioSource.Play();
        }

        private void OnDestroy()
        {
            pickableEvents.OnProjectileChange -= HandleProjectileChange;
        }
    }
}
