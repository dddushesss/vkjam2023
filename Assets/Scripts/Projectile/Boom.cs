using UnityEngine;

namespace Projectile
{
    public class Boom : MonoBehaviour
    {
        [SerializeField] private float lifeTime;
    
        private void Start()
        {
            Invoke(nameof(Destroy), lifeTime);
        }

        private void Destroy()
        {
            Destroy(gameObject);
        }
    }
}
