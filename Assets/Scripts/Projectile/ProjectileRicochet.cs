﻿using Damageable;
using UnityEngine;

namespace Projectile
{
    public class ProjectileRicochet : Projectile
    {
        private bool _hasRicocheted;

        protected override void ApplyDamage(IDamageable damageable, Collision2D col)
        {
            damageable.Damage(damagePoints);

            if (!_hasRicocheted) Ricochet(col);
            else Destroy();
        }

        private void Ricochet(Collision2D col)
        {
            _hasRicocheted = true;
            
            float x = 0;
            float y = 0;
            foreach (var contact in col.contacts)
            {
                x += contact.normal.x;
                y += contact.normal.y;
            }
            var centroidNormal = new Vector2(x / col.contactCount, y / col.contactCount).normalized;
            var angle = Vector2.SignedAngle(transform.right, - centroidNormal);
            transform.Rotate(0, 0, 2 * (angle - 90));
        }
    }
}