using System;
using Damageable;
using UnityEngine;

namespace Projectile
{
    public class Projectile : Damager
    {
        [SerializeField] protected float speed = 10;
        [SerializeField] protected float lifetime = 1;

        protected override void Init()
        {
            base.Init();
            
            Invoke(nameof(Destroy), lifetime);
        }

        private void FixedUpdate()
        {
            gameObject.transform.position += Time.fixedDeltaTime * speed * transform.right;
        }

        private void OnDestroy()
        {
            CancelInvoke();
        }
    }
}
