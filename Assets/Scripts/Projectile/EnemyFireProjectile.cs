using Enemies;
using UnityEngine;

namespace Projectile
{
    public class EnemyFireProjectile : FireProjectile
    {
        [SerializeField] private float fireRate;
        
        private AreaDetection _areaDetection;
        private Collider2D _target;
        private Animator _animator;
        private static readonly int Attack = Animator.StringToHash("Attack");

        protected override void Init()
        {
            base.Init();
            
            _areaDetection = transform.GetComponentInChildren<AreaDetection>();
            _areaDetection.OnEnter += HandleOnPlayerEnter;
            _areaDetection.OnExit += HandleOnPlayerExit;
            _animator = GetComponent<Animator>();
            
            InvokeRepeating(nameof(FireWithArgs), 0f, fireRate);
        }

        private void HandleOnPlayerEnter(Collider2D playerCol)
        {
            _target = playerCol;
        }

        private void HandleOnPlayerExit(Collider2D playerCol)
        {
            _target = null;
        }

        private void FireWithArgs()
        {
            if (_target == null) return;
            _animator.SetTrigger(Attack);

        }

        private void OnDestroy()
        {
            _areaDetection.OnEnter -= HandleOnPlayerEnter;
            _areaDetection.OnExit -= HandleOnPlayerExit;

            CancelInvoke();
        }

        //Used in animation
        public void ThrowBall()
        {
            if (_target == null) return;
            var dir = _target.transform.position - transform.position;
            Fire(gameObject.transform.position, dir);
        }
    }
}
