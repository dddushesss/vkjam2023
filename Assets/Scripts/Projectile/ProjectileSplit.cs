﻿using UnityEngine;

namespace Projectile
{
    public class ProjectileSplit : Projectile
    {
        [SerializeField] private GameObject tale;

        protected override void Init()
        {
            Invoke(nameof(Split), lifetime);
        }

        private void Split()
        {
            tale.transform.position = transform.position;
            tale.transform.rotation = transform.rotation;
            tale.SetActive(true);
            
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            CancelInvoke();
        }
    }
}