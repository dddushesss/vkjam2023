using System;
using System.Collections.Generic;
using UnityEngine;

namespace Projectile
{
    [CreateAssetMenu(fileName = "ProjectilesScriptableObject", menuName = "ProjectilesScriptableObject", order = 51)]
    public class ProjectilesSO : ScriptableObject
    {
        [Serializable]
        class ProjectileDescription
        {
            public string name;
            public GameObject prefab;
        }
        
        [SerializeField] private List<ProjectileDescription> projectiles;

        public readonly Dictionary<string, GameObject> Projectiles = new();

        public void Init()
        {
            if (Projectiles.Count > 0) return;
            
            foreach (var p in projectiles)
            {
                Projectiles[p.name] = p.prefab;
            }
        }
    }
}
