﻿using System.Collections.Generic;
using Damageable;
using Enemies;
using UnityEngine;

namespace Projectile
{
    public class ProjectileExplosion : Projectile
    {
        [SerializeField] private AreaDetection area;
        [SerializeField] private GameObject boom;

        private readonly List<IDamageable> _damageableInArea = new();

        protected override void Init()
        {
            area.OnEnter += HandleOnEnter;
            area.OnExit += HandleOnExit;
        }

        private void HandleOnEnter(Collider2D col)
        {
            if (!col.gameObject.TryGetComponent<IDamageable>(out var damageable)) return;
            _damageableInArea.Add(damageable);
        }

        private void HandleOnExit(Collider2D col)
        {
            if (!col.gameObject.TryGetComponent<IDamageable>(out var damageable)) return;
            _damageableInArea.Remove(damageable);
        }

        protected override void ApplyDamage(IDamageable damageable, Collision2D col)
        {
            foreach (var d in _damageableInArea.ToArray())
            {
                d.Damage(damagePoints);
            }

            Instantiate(boom, transform.position, transform.rotation);
            Destroy();
        }

        private void OnDestroy()
        {
            area.OnEnter -= HandleOnEnter;
            area.OnExit -= HandleOnExit;
        }
    }
}