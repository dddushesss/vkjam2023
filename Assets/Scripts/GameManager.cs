using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameEvents gameEvents;

    private void Start()
    {
        if (gameEvents == null) Debug.LogError("gameEvents is not assigned", this);

        gameEvents.OnPlayerDied += RestartGame;
        gameEvents.OnEndGame += NextLevel;
    }

    [EditorButton]
    private void RestartGame()
    {
        Debug.Log("Restarting the game");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    [EditorButton]
    private void LoadLevel(int index)
    {
        SceneManager.LoadScene(index);
    }
    private void NextLevel()
    {
        if (SceneManager.GetActiveScene().buildIndex + 1 > SceneManager.sceneCount)
        {
            return;
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void OnDestroy()
    {
        gameEvents.OnPlayerDied -= RestartGame;
        gameEvents.OnEndGame -= NextLevel;
    }
}
