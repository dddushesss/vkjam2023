﻿using System;
using UnityEngine;

namespace Pickables
{
    [CreateAssetMenu(fileName = "PickableEvents", menuName = "PickableEvents", order = 51)]
    public class PickableEvents : ScriptableObject
    {
        public Action<float> OnHealthAdd;
        public Action<string> OnProjectileChange;
    }
}