﻿using UnityEngine;

namespace Pickables
{
    public class PickableSpawner : MonoBehaviour
    {
        private void Start()
        {
            if (PickableFactory.Instance == null)
            {
                Debug.LogError("Set PickableFactory in the scene");
            }
            
            Spawn();
        }

        public void Spawn()
        {
            var pickable = PickableFactory.Instance.CreateRandomPickable();
            pickable.transform.position = transform.position;
            Debug.Log($"PickableSpawner spawned {pickable.name}");
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawIcon(transform.position, "PickableSpawner");
        }
    }
}