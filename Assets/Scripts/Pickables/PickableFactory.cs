﻿using System.Linq;
using UnityEngine;

namespace Pickables
{
    public class PickableFactory : MonoBehaviour
    {
        [SerializeField] private PickableScriptableObject pickableScriptableObject;
        
        public static PickableFactory Instance { get; protected set; }

        private System.Random _rand;
        
        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(this);
                throw new System.Exception("An instance of this singleton already exists.");
            }
            else
            {
                Instance = this;
            }
            
            pickableScriptableObject.Init();
            _rand = new System.Random();
        }

        public GameObject CreatePickable(string pickableName)
        {
            return Instantiate(pickableScriptableObject.Pickables[pickableName], transform);
        }

        public GameObject CreateRandomPickable()
        {
            if (pickableScriptableObject.Pickables.Count <= 0)
            {
                Debug.LogError("No pickables in PickableScriptableObject");
                return new GameObject();
            }
            
            var index = _rand.Next(0, pickableScriptableObject.Pickables.Count);
            return Instantiate(pickableScriptableObject.Pickables.ElementAt(index).Value, transform);
        }
    }
}