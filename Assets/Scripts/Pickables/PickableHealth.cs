﻿using UnityEngine;

namespace Pickables
{
    public class PickableHealth : MonoBehaviour
    {
        [SerializeField] private PickableEvents pickableEvents;
        [SerializeField] private float addHealthPoints = 50;

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.isTrigger) return;
            pickableEvents.OnHealthAdd?.Invoke(addHealthPoints);
            Destroy(gameObject);
        }
    }
}