﻿using UnityEngine;

namespace Pickables
{
    public class PickableProjectile : MonoBehaviour
    {
        [SerializeField] private PickableEvents pickableEvents;
        [SerializeField] private string changeProjectile = "base";
        
        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.isTrigger) return;
            pickableEvents.OnProjectileChange?.Invoke(changeProjectile);
            Destroy(gameObject);
        }
    }
}