﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pickables
{
    [CreateAssetMenu(fileName = "PickableScriptableObject", menuName = "PickableScriptableObject", order = 51)]
    public class PickableScriptableObject : ScriptableObject
    {
        [Serializable]
        class PickableDescription
        {
            public string name;
            public GameObject prefab;
        }
        
        [SerializeField] private List<PickableDescription> pickables;

        public readonly Dictionary<string, GameObject> Pickables = new();

        public void Init()
        {
            if (Pickables.Count > 0) return;
            
            foreach (var p in pickables)
            {
                Pickables[p.name] = p.prefab;
            }
        }
    }
}