﻿using System;
using System.Linq;
using DefaultNamespace;
using UnityEngine;

namespace Level
{
    public class TalkerSpawner : MonoBehaviour
    {
        [SerializeField] private PropsTypeLibrary library;

        private void Start()
        {
            var type = GlobalStorage.Instance.LevelGenerator.GetRoomByPosition(transform.position);
            var preset = library.PropsTypes.First(x => x.Type == type);
            Instantiate(preset.Sprite, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}