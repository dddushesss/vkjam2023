﻿using System;
using Enemies;
using UnityEngine;

namespace Level
{
    [CreateAssetMenu(fileName = "BackgroundLibrary", menuName = "Level/Background lib", order = 0)]
    public class BackgroundLibrary : ScriptableObject
    {
        [SerializeField] private BackgroundPreset[] presets;
        
        public BackgroundPreset[] Presets => presets;
    }

    [Serializable]
    public class BackgroundPreset
    {
        [SerializeField] private BossTypes type;
        [SerializeField] private Sprite[] sprites;
        
        public Sprite[] Sprites => sprites;
        public BossTypes Type => type;
    }
}