using System;
using System.IO;
using DefaultNamespace;
using Enemies;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Level
{
    public class PropView : MonoBehaviour
    {
        [SerializeField] private PropType type;

        private const string PathToData = "Assets/ScriptableObjects/";
        private const string DataName = "PropsTypeLibrary";

        private PropsTypeLibrary _library;
        private BossTypes _roomType; 

        private void Start()
        {
            _library = GlobalStorage.Instance.PropsTypeLibrary;
            _roomType = GlobalStorage.Instance.LevelGenerator.GetRoomByPosition(transform.position);
            var types = _library.PropsTypes.FindAll(settings =>
                settings.PropType == type 
                && settings.Type == _roomType);
            if (types.Count == 0)
            {
                Debug.LogWarning("No type found for " + type + " at " + _roomType);
                return;
            }
            var settings = types[Random.Range(0, types.Count - 1)];
            if (settings.Sprite == null)
            {
                return;
            }
            Instantiate(settings.Sprite, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

        private void OnDrawGizmos()
        {
            switch (_roomType)
            {
                case BossTypes.NONE:
                    break;
                case BossTypes.STARVE:
                    Gizmos.color = Color.cyan;
                    break;
                case BossTypes.WAR:
                    Gizmos.color = Color.red;
                    break;
                case BossTypes.DEATH:
                    Gizmos.color = Color.yellow;
                    break;
                case BossTypes.PLAGUE:
                    Gizmos.color = Color.green;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            Gizmos.DrawCube(transform.position, Vector3.one);
        }
    }
}