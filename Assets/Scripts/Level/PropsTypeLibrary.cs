﻿using System;
using System.Collections.Generic;
using Enemies;
using UnityEngine;

namespace Level
{
    [CreateAssetMenu(fileName = "PropsTypeLibrary", menuName = "Props/Props Type Library", order = 0)]
    public class PropsTypeLibrary : ScriptableObject
    {
        [SerializeField] private List<PropTypeSettings> propsTypes;

        public List<PropTypeSettings> PropsTypes => propsTypes;
    }

    [Serializable]
    public class PropTypeSettings
    {
        [SerializeField] private PropType propType;
        [SerializeField] private BossTypes type;
        [SerializeField] private GameObject sprite;

        public PropType PropType => propType;

        public BossTypes Type => type;

        public GameObject Sprite => sprite;

    }

    public enum PropType
    {
        WINDOW,
        CABINET,
        COLUM_UPPER,
        COLUM_LOWER,
        COLUM_MIDDLE,
        BACKGROUND
    }
}