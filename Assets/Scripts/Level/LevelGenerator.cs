using System;
using System.Collections.Generic;
using Enemies;
using UnityEngine;

namespace Level
{
    public class LevelGenerator : MonoBehaviour
    {
        public Action<BossTypes> OnBossAreaChanged;
        
        [SerializeField] private RoomView[] fiilers;
        [SerializeField] private RoomView[] bossRooms;
        
        [SerializeField] private int height;
        [SerializeField] private int width;

        private RoomView currentRoom;
        private BossTypes _currentBossType = BossTypes.NONE;
        private RoomView[,] spawnedRoom;

        private float yRoomStep;
        private float xRoomStep;

        private void Start()
        {
            GenerateLevel();
        }

        private void OnDestroy()
        {
            foreach (var room in spawnedRoom)
            {
                if (room == null || room.OnBossAreaEntered == null)
                {
                    continue;
                }
                room.OnBossAreaEntered -= HandleOnPlayerEnterRoom;
            }
        }

        [EditorButton]
        private void GenerateLevel()
        {
            var currentPosition = Vector2.zero;
            var isPlayerSpawn = false;
            var bossRoomList = new List<RoomView>(4);
            spawnedRoom = new RoomView[height, width];
            
            for (var i = 0; i < 4; i++)
            {
                bossRoomList.Add(bossRooms[i]);
            }
            
            for (var i = 0; i < height; i++)
            {
                for (var j = 0; j < width; j++)
                {
                    if ((i == 0 && j == 0) 
                        || (i == height - 1 && j == 0) 
                        || (i == 0 && j == width - 1) 
                        || (i == height - 1 && j == width - 1))
                    {
                        currentRoom = Instantiate(bossRoomList.PopRand(), currentPosition, Quaternion.identity);
                    }
                    
                    else
                    {
                        currentRoom = Instantiate(fiilers.GetRand(), currentPosition, Quaternion.identity);
                    }

                    currentRoom.OnBossAreaEntered += HandleOnPlayerEnterRoom;
                    if ((height + 1)  / (i + 1)  == 2 && (width + 1) / (j + 1) == 2 && !isPlayerSpawn)
                    {
                        currentRoom.GetSpawner().Spawn();
                        isPlayerSpawn = true;
                    }
                    
                    if (i > 0)
                    {
                        currentRoom.GetUpBorder().SetActive(false);
                    }

                    if (i < height - 1)
                    {
                        currentRoom.GetDownBorder().SetActive(false);
                    }
                    
                    if (j > 0)
                    {
                        currentRoom.GetLeftBorder().SetActive(false);
                    }
                    
                    if (j < width - 1)
                    {
                        currentRoom.GetRightBorder().SetActive(false);
                    }

                    if ((i == 0 && j == 0) || (i == height - 1 && j == 0))
                    {
                        currentRoom.BossSpriteRenderer.flipX = true;
                    }

                    spawnedRoom[i, j] = currentRoom;
                    xRoomStep = currentRoom.GetHorizontalSize();
                    currentPosition += new Vector2(currentRoom.GetHorizontalSize(), 0);
                }
                currentPosition.x = 0;
                currentPosition -= new Vector2(0, currentRoom.GetVerticalSize());
                yRoomStep = currentRoom.GetVerticalSize();
            }

            for (var i = 0; i < height/2; i++)
            {
                for (var j = 0; j < width/2; j++)
                {
                    var type = spawnedRoom[0, 0].GetBossType();
                    spawnedRoom[i, j].SetBossType(type);
                }

                for (var j =  width / 2; j < width; j++)
                {
                    var type = spawnedRoom[0, width - 1].GetBossType();
                    spawnedRoom[i, j].SetBossType(type);
                }
            }
            for (var i =  height / 2; i < height; i++)
            {
                for (var j = 0; j < width / 2; j++)
                {
                    var type = spawnedRoom[height - 1, 0].GetBossType();
                    spawnedRoom[i, j].SetBossType(type);
                }

                for (var j = width/2; j < width; j++)
                {
                    var type = spawnedRoom[height - 1, width - 1].GetBossType();
                    spawnedRoom[i, j].SetBossType(type);
                }
            }
        }
        private void HandleOnPlayerEnterRoom(BossTypes type)
        {
            if (_currentBossType == type) return;
            _currentBossType = type;
            OnBossAreaChanged?.Invoke(type);
            Debug.Log("Entered room type : " + type);
        }

        public BossTypes GetRoomByPosition(Vector2 position)
        {
            var h = Mathf.FloorToInt(Mathf.Abs((position.y - yRoomStep/2) / (yRoomStep)));
            var w = Mathf.FloorToInt(Mathf.Abs((position.x - 2f + xRoomStep/2) / (xRoomStep)));
            return spawnedRoom[h, w].GetBossType();
        }
    }
}
