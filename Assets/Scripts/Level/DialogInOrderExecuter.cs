﻿using UnityEngine;

namespace Level
{
    public class DialogInOrderExecuter : MonoBehaviour
    {
        [SerializeField] private DialogUser[] dialogUsers;
        [SerializeField] private DialogManager manager;

        private int _currentUser = 0;
        public void Start()
        {
            manager.OnDialogEnds += HandleOnDialogEnds;
            HandleOnDialogEnds();
        }

        private void HandleOnDialogEnds()
        {
            if (_currentUser > dialogUsers.Length - 1)
            {
                Application.Quit();
                return;
            }
            manager.StartDialog(dialogUsers[_currentUser++]);
        }

    }
}