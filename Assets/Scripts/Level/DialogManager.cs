﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Level
{
    public class DialogManager : MonoBehaviour
    {
        public Action OnDialogEnds;
        
        [SerializeField] private Image backImage;
        [SerializeField] private Text text;
        [SerializeField] private Canvas canvas;
        [SerializeField] private KeyCode enterKey = KeyCode.F;
        [SerializeField] private DialogLibrary library;
        [SerializeField] private Vector3 offset;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private DialogType[] dialogTypesToEnd;
        [SerializeField] private GameEvents gameEvents;
        
        private float fraseTime = 3f;

        private int _currentFrase;
        private bool _isDialogStarted;
        private float _lastTime;
        private Dictionary<DialogType, int> _currentDialog = new();
        private Dictionary<DialogUser, int> _userDialog = new();
        private DialogUser currentUser;
        private List<DialogType> _toldedDialogs;
        private int _sceneIndex = -1;
        
        private void Awake()
        {
            var managers = FindObjectsOfType<DialogManager>();
            if (managers.Any(manager => manager.GetScenesIndex() == SceneManager.GetActiveScene().buildIndex))
            {
                Destroy(gameObject);
                return;
            }
            _sceneIndex = SceneManager.GetActiveScene().buildIndex;
            DontDestroyOnLoad(gameObject);
        }

        public int GetScenesIndex()
        {
            return _sceneIndex;
        }
        
        private void Start()
        {
            _toldedDialogs = dialogTypesToEnd.ToList();
            if (_currentDialog.Count > 0)
            {
                return;
            }
            foreach (var type in Enum.GetValues(typeof(DialogType)).Cast<DialogType>())
            {
                _currentDialog.Add(type, 0);
            }
        }

        private void Update()
        {
            if (!(Time.time > _lastTime + fraseTime) || !_isDialogStarted) return;

            CloseDialog();
        }

        public void HandleOnStayInCollider(DialogUser user)
        {
            if (currentUser != null && currentUser != user && _isDialogStarted && !Input.GetKeyDown(enterKey))
            {
                return;
            }
            if (!Input.GetKeyDown(enterKey) && !_isDialogStarted)
            {
                return;
            }

            if (currentUser != null && currentUser != user)
            {
                CloseDialog(true);
            }

            currentUser = user;
            if (!_isDialogStarted)
            {
                StartDialog();
                audioSource.Play();
            }

            canvas.transform.position = user.transform.position + offset;
        }

        private void StartDialog()
        {
            var preset = library.Dialogs.First(x => x.Type == currentUser.GetDialogType());
            canvas.transform.position = currentUser.transform.position + offset;
            audioSource.clip = preset.TalkSound;
            if (!_userDialog.ContainsKey(currentUser))
            {
                _userDialog.Add(currentUser, _currentDialog[currentUser.GetDialogType()]);
                _currentDialog[currentUser.GetDialogType()] =
                    _currentDialog[currentUser.GetDialogType()] >= preset.Frases.Count() - 1
                        ? 0
                        : _currentDialog[currentUser.GetDialogType()] + 1;
            }
            canvas.gameObject.SetActive(true);
            text.text = preset.Frases[_userDialog[currentUser]].Frases[_currentFrase].Text;
            fraseTime = preset.Frases[_userDialog[currentUser]].Frases[_currentFrase].Time;
            text.font = preset.Font;
            _lastTime = Time.time;
            _isDialogStarted = true;
        }
        
        public void StartDialog(DialogUser user)
        {
            currentUser = user;
            StartDialog();
        }

        private void CloseDialog(bool isForce = false)
        {
            var preset = library.Dialogs.First(x => x.Type == currentUser.GetDialogType());
            if (_currentFrase < preset.Frases[_userDialog[currentUser]].Frases.Length - 1 && !isForce)
            {
                _currentFrase++;
                StartDialog();
                return;
            }

            if (dialogTypesToEnd.Contains(currentUser.GetDialogType()))
            {
                _toldedDialogs.Remove(currentUser.GetDialogType());
                if (_toldedDialogs.Count == 0)
                {
                    gameEvents.OnEndGame?.Invoke();
                }
            }
            
            audioSource.Stop();
            canvas.gameObject.SetActive(false);
            _isDialogStarted = false;
            _currentFrase = 0;
            OnDialogEnds?.Invoke();
        }
    }
}