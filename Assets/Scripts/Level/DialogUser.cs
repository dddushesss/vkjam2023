﻿using System;
using DefaultNamespace;
using UnityEngine;

namespace Level
{
    [RequireComponent(typeof(Collider2D))]
    public class DialogUser : MonoBehaviour
    {
        [SerializeField] private DialogType _dialogType;
        private void OnTriggerStay2D(Collider2D other)
        {
            if (!other.CompareTag("Player"))
                return;
            GlobalStorage.Instance.DialogManager.HandleOnStayInCollider(this);
        }

        public DialogType GetDialogType()
        {
            return _dialogType;
        }
    }
}