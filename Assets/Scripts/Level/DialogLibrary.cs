﻿using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Level
{
    [CreateAssetMenu(fileName = "DialogLibrary", menuName = "Dialog Library", order = 0)]
    public class DialogLibrary : ScriptableObject
    {
        [SerializeField] private DialogSettings[] dialogs;
        
        public DialogSettings[] Dialogs => dialogs;
    }

    [Serializable]
    public class DialogSettings
    {
        [SerializeField] private DialogType type;
        [SerializeField] private Frase[] frases;
        [SerializeField] private Font font;
        [SerializeField] private AudioClip talkSound;
        
        public DialogType Type => type;
        public Frase[] Frases => frases;
        public Font Font => font;
        public AudioClip TalkSound => talkSound;
    }

    [Serializable]
    public class Frase
    {
        [SerializeField] private Dialog[] frases;
        
        public Dialog[] Frases => frases;
    }
    [Serializable]
    public class Dialog
    {
        [SerializeField] private string text;
        [SerializeField] private float time;
        
        public string Text => text;
        public float Time => time;
    }
    public enum DialogType
    {
        PLAGUE,
        DEATH,
        STARVE,
        WAR,
        SKULL,
        DOLL,
        ELEPHANT,
        MASK
    }
}