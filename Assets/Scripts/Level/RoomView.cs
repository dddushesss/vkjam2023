﻿using System;
using System.Linq;
using Character;
using DefaultNamespace;
using Enemies;
using ShowIf;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Level
{
    public class RoomView : MonoBehaviour
    {
        public Action<BossTypes> OnBossAreaEntered;
        
        [SerializeField] private CharacterSpawner _characterSpawner;
        [SerializeField] private GameObject upBorder;
        [SerializeField] private GameObject downBorder;
        [SerializeField] private GameObject leftBorder;
        [SerializeField] private GameObject rightBorder;
        [SerializeField] private bool isBoosRoom;

        [SerializeField] [ShowIf(ActionOnConditionFail.DontDraw, ConditionOperator.And, "isBoosRoom")]
        private BossTypes bossType;
        
        [SerializeField] [ShowIf(ActionOnConditionFail.DontDraw, ConditionOperator.And, "isBoosRoom")]
        private GameObject bossGameObject;
        
        [SerializeField] private string bossName;

        public SpriteRenderer BossSpriteRenderer => bossGameObject.GetComponent<SpriteRenderer>();
        
        private void Awake()
        {
            bossName = bossType.ToString();
            foreach (var go in gameObject.GetAllChilderns())
            {
                go.TryGetComponent<Rigidbody2D>(out var rb);
                if (rb != null)
                {
                    rb.bodyType = RigidbodyType2D.Static;
                }
            }
        }

        private void Start()
        {
            var backList = GlobalStorage.Instance.BacksTypeLibrary.Presets.First(x => x.Type == GetBossType()).Sprites;

            var back = backList.GetRand();
            var x = GetLeftBorder().transform.position.x;
            var y = GetUpBorder().transform.position.y;
            var xSize = 0f;
            while (x < GetRightBorder().transform.position.x)
            {
                while (y > GetDownBorder().transform.position.y)
                {
                    var tileY = new GameObject
                    {
                        transform =
                        {
                            position = new Vector2(x, y)
                        },
                        name = "BackTile" + GetBossType()
                    };
                    tileY.transform.SetParent(transform);
                    var spriteRendererY = tileY.AddComponent<SpriteRenderer>();
                    spriteRendererY.sprite = back;
                    spriteRendererY.sortingOrder = -10;
                    y -= spriteRendererY.size.y;
                    xSize = spriteRendererY.size.x;
                }
                x += xSize;
                y = GetUpBorder().transform.position.y;
            }

        }

        public CharacterSpawner GetSpawner()
        {
            return _characterSpawner;
        }
        
        public BossTypes GetBossType()
        {
            return bossType;
        }

        public void SetBossType(BossTypes bossType)
        {
            if(isBoosRoom)
                return;
            bossName = bossType.ToString();
            this.bossType = bossType;
        }
        
        public GameObject GetUpBorder()
        {
            return upBorder;
        }

        public GameObject GetDownBorder()
        {
            return downBorder;
        }

        public GameObject GetLeftBorder()
        {
            return leftBorder;
        }

        public GameObject GetRightBorder()
        {
            return rightBorder;
        }

        public float GetHorizontalSize()
        {
            var rightSpriteRenderer = GetRightBorder().GetComponent<SpriteRenderer>();
            return Mathf.Abs(GetRightBorder().transform.position.x - GetLeftBorder().transform.position.x - rightSpriteRenderer.size.x);
        }

        public float GetVerticalSize()
        {
            var upSpriteRenderer = GetUpBorder().GetComponent<SpriteRenderer>();
            return Mathf.Abs(GetUpBorder().transform.position.y - GetDownBorder().transform.position.y - upSpriteRenderer.size.y);
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("Player"))
            {
                OnBossAreaEntered(GetBossType());
            }
        }
    }
}